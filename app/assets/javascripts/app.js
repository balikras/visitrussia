(function () {
  var app = angular.module("visitRussia", ["ngResource", 'ngSanitize']);

  app.controller('MainController', ['$scope', '$http', '$interval', '$sce', function($scope, $http, $interval, $sce) {

    $http({
      method: 'GET',
      url: 'https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
    }).then(function successCallback(response) {
      $scope.usdRate = response.data.query.results.rate[0].Ask;
      }, function errorCallback(response) {
        $scope.usdRate = response;
      });

    $http({
      method: 'GET',
      url: 'titles'
    }).then(function successCallback(response) {
      $scope.titles = response.data;
      }, function errorCallback(response) {
        $scope.titles = response;
      });

    $interval(function() {
      $http({
        method: 'GET',
        url: 'https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='
      }).then(function successCallback(response) {
        $scope.usdRate = response.data.query.results.rate[0].Ask;
        }, function errorCallback(response) {
          $scope.usdRate = response;
        });
      }, 10000);

    $http({
      method: 'GET',
      url: 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22moscow%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys'
    }).then(function successCallback(response) {
      $scope.weather = response.data.query.results.channel.item.condition.temp;
      }, function errorCallback(response) {
        $scope.weather = response;
      });

      var language = window.navigator.userLanguage || window.navigator.language;
      if(language == 'ru-ru'){
        $scope.contactText = 'Хотите разместиться здесь? <a href="mailto:youzik@me.com"> Напишите нам!</a>'
      }else{
        $scope.contactText = 'Want your link here? <a href="mailto:youzik@me.com"> Сontact us! </a>'
      };

    }]);

})();
