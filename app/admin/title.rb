ActiveAdmin.register Title do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :body, :title, :link
#
# or
#
# permit_params do
#   permitted = [:body, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
