# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160127092456) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "advisor_bonuses", force: true do |t|
    t.integer  "advisor_id"
    t.integer  "bonus_entry_id"
    t.string   "description"
    t.decimal  "amount",            precision: 8, scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "admin_description"
  end

  add_index "advisor_bonuses", ["advisor_id"], name: "index_advisor_bonuses_on_advisor_id", using: :btree

  create_table "advisor_categories", force: true do |t|
    t.integer  "advisor_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "advisor_categories", ["advisor_id"], name: "index_advisor_categories_on_advisor_id", using: :btree
  add_index "advisor_categories", ["category_id"], name: "index_advisor_categories_on_category_id", using: :btree

  create_table "advisor_discounts", force: true do |t|
    t.integer  "user_id"
    t.integer  "advisor_id"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "advisor_discounts", ["user_id", "expires_at", "advisor_id"], name: "ad_search_index", using: :btree

  create_table "advisor_trends", force: true do |t|
    t.integer "advisor_id"
    t.integer "previous_position"
    t.integer "current_position"
    t.integer "notch"
    t.integer "day_1",             default: 0, null: false
    t.integer "day_2",             default: 0, null: false
    t.integer "day_3",             default: 0, null: false
    t.integer "day_4",             default: 0, null: false
    t.integer "day_5",             default: 0, null: false
    t.integer "day_6",             default: 0, null: false
    t.integer "day_7",             default: 0, null: false
    t.integer "day_8",             default: 0, null: false
    t.integer "day_9",             default: 0, null: false
    t.integer "day_10",            default: 0, null: false
    t.integer "day_11",            default: 0, null: false
    t.integer "day_12",            default: 0, null: false
    t.integer "day_13",            default: 0, null: false
    t.integer "day_14",            default: 0, null: false
    t.integer "day_15",            default: 0, null: false
    t.integer "day_16",            default: 0, null: false
    t.integer "day_17",            default: 0, null: false
    t.integer "day_18",            default: 0, null: false
    t.integer "day_19",            default: 0, null: false
    t.integer "day_20",            default: 0, null: false
    t.integer "day_21",            default: 0, null: false
    t.integer "day_22",            default: 0, null: false
    t.integer "day_23",            default: 0, null: false
    t.integer "day_24",            default: 0, null: false
    t.integer "day_25",            default: 0, null: false
    t.integer "day_26",            default: 0, null: false
    t.integer "day_27",            default: 0, null: false
    t.integer "day_28",            default: 0, null: false
    t.integer "day_29",            default: 0, null: false
    t.integer "day_30",            default: 0, null: false
    t.integer "day_31",            default: 0, null: false
  end

  add_index "advisor_trends", ["advisor_id"], name: "index_advisor_trends_on_advisor_id", using: :btree
  add_index "advisor_trends", ["notch", "advisor_id"], name: "index_advisor_trends_on_notch_and_advisor_id", using: :btree

  create_table "advisors", force: true do |t|
    t.integer  "user_id"
    t.string   "profile_picture_uid"
    t.string   "profile_video_uid"
    t.string   "service_title"
    t.text     "service_description"
    t.text     "instructions"
    t.boolean  "is_available",                                                          default: true
    t.integer  "likes_count",                                                           default: 0
    t.integer  "dislikes_count",                                                        default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "profile_video_thumb_uid"
    t.string   "state",                            limit: 32
    t.string   "rejection_reason",                 limit: 1024
    t.string   "bitwine_username"
    t.string   "bitwine_url"
    t.decimal  "happiness",                                     precision: 5, scale: 2
    t.text     "about_me"
    t.decimal  "sort_order",                                    precision: 4, scale: 2
    t.string   "paypal_email"
    t.string   "credentials",                      limit: 8192
    t.integer  "average_response_seconds",                                              default: 0
    t.datetime "rush_delivery_expires_at"
    t.datetime "rush_delivery_disabled_until"
    t.datetime "thrown_to_unavailable_at"
    t.datetime "not_available_since"
    t.string   "suspension_reason",                limit: 1024
    t.datetime "suspended_until"
    t.integer  "missed_orders_count",                                                   default: 0,     null: false
    t.string   "bitwine_code",                     limit: 6
    t.integer  "ontime_delivered_count",                                                default: 0,     null: false
    t.datetime "suspended_at"
    t.string   "w89_form_uri"
    t.string   "profile_wide_picture_uid"
    t.boolean  "staff_pick",                                                            default: false
    t.text     "staff_pick_text"
    t.string   "video_os"
    t.boolean  "automatic_payout",                                                      default: false, null: false
    t.string   "country_of_citizenship_code",      limit: 2
    t.string   "business_name"
    t.string   "federal_tax_classification"
    t.string   "ssn",                              limit: 9
    t.string   "ein",                              limit: 9
    t.date     "date_of_signature"
    t.string   "foreign_tax_id_number"
    t.string   "reference_number"
    t.string   "mailing_address"
    t.string   "mailing_city"
    t.string   "mailing_country_code",             limit: 2
    t.string   "photo_uid"
    t.string   "photo_id_iv",                      limit: 24
    t.string   "federal_tax_classification_other"
    t.text     "termination_reason"
    t.integer  "rush_delivery_disabled_count",                                          default: 0,     null: false
    t.integer  "suspended_count",                                                       default: 0,     null: false
    t.datetime "rejected_at"
    t.datetime "applied_at"
    t.integer  "queued",                                                                default: 0,     null: false
    t.integer  "queued_rush",                                                           default: 0,     null: false
  end

  add_index "advisors", ["automatic_payout"], name: "index_advisors_on_automatic_payout", using: :btree
  add_index "advisors", ["ein"], name: "index_advisors_on_ein", unique: true, using: :btree
  add_index "advisors", ["foreign_tax_id_number"], name: "index_advisors_on_foreign_tax_id_number", unique: true, using: :btree
  add_index "advisors", ["service_title", "service_description"], name: "index_advisors_on_service_title_and_service_description", using: :btree
  add_index "advisors", ["sort_order", "state", "is_available"], name: "index_advisors_on_sort_order_and_state_and_is_available", using: :btree
  add_index "advisors", ["ssn"], name: "index_advisors_on_ssn", unique: true, using: :btree
  add_index "advisors", ["state"], name: "index_advisors_on_state", using: :btree
  add_index "advisors", ["user_id"], name: "index_advisors_on_user_id", unique: true, using: :btree

  create_table "androiders", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "apple_payins", force: true do |t|
    t.string   "details"
    t.decimal  "amount",                 precision: 20, scale: 2, default: 0.0, null: false
    t.decimal  "fees_amount",            precision: 20, scale: 2, default: 0.0, null: false
    t.date     "paid_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",    limit: 32
  end

  add_index "apple_payins", ["provider"], name: "index_apple_payins_on_provider", using: :btree

  create_table "audits", force: true do |t|
    t.integer  "reference_id",                             null: false
    t.string   "reference_type",                           null: false
    t.string   "event",          limit: 32,                null: false
    t.text     "request"
    t.boolean  "success",                   default: true
    t.text     "response"
    t.text     "comment"
    t.text     "error"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "audits", ["event", "success"], name: "index_audits_on_event_and_success", using: :btree
  add_index "audits", ["reference_id"], name: "index_audits_on_reference_id", using: :btree
  add_index "audits", ["success"], name: "index_audits_on_success", using: :btree

  create_table "bonuses", force: true do |t|
    t.integer  "order_id",                                      null: false
    t.decimal  "price",                precision: 8,  scale: 2, null: false
    t.decimal  "advisor_price",        precision: 18, scale: 2
    t.decimal  "other_fees",           precision: 18, scale: 2
    t.integer  "fulfill_entry_id"
    t.integer  "unfulfill_entry_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "fulfilled_at"
    t.integer  "clearance_entry_id"
    t.datetime "cleared_at"
    t.integer  "unclearance_entry_id"
  end

  add_index "bonuses", ["order_id"], name: "index_bonuses_on_order_id", using: :btree

  create_table "card_prices", force: true do |t|
    t.decimal  "price",         precision: 8, scale: 2, null: false
    t.decimal  "credit_gained", precision: 8, scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "card_templates", force: true do |t|
    t.string   "name"
    t.string   "image_uid"
    t.boolean  "available",  default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "icon_uid"
    t.string   "hires_icon_uid"
    t.string   "icon_large_uid"
    t.string   "icon_white_uid"
    t.string   "icon_white_large_uid"
    t.integer  "sort_order",           default: 1000, null: false
  end

  add_index "categories", ["name"], name: "index_categories_on_name", unique: true, using: :btree
  add_index "categories", ["sort_order"], name: "index_categories_on_sort_order", unique: true, using: :btree

  create_table "chargebacks", force: true do |t|
    t.string   "creditable_type", limit: 24
    t.integer  "creditable_id"
    t.text     "admin_reason"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "chargebacks", ["creditable_type", "creditable_id"], name: "index_chargebacks_on_creditable_type_and_creditable_id", using: :btree

  create_table "client_logs", force: true do |t|
    t.string   "deviceid"
    t.text     "body"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "client_logs", ["deviceid"], name: "index_client_logs_on_deviceid", using: :btree
  add_index "client_logs", ["user_id"], name: "index_client_logs_on_user_id", using: :btree

  create_table "comfy_cms_blocks", force: true do |t|
    t.string   "identifier",     null: false
    t.text     "content"
    t.integer  "blockable_id"
    t.string   "blockable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comfy_cms_blocks", ["blockable_id", "blockable_type"], name: "index_comfy_cms_blocks_on_blockable_id_and_blockable_type", using: :btree
  add_index "comfy_cms_blocks", ["identifier"], name: "index_comfy_cms_blocks_on_identifier", using: :btree

  create_table "comfy_cms_categories", force: true do |t|
    t.integer "site_id",          null: false
    t.string  "label",            null: false
    t.string  "categorized_type", null: false
  end

  add_index "comfy_cms_categories", ["site_id", "categorized_type", "label"], name: "index_cms_categories_on_site_id_and_cat_type_and_label", unique: true, using: :btree

  create_table "comfy_cms_categorizations", force: true do |t|
    t.integer "category_id",      null: false
    t.string  "categorized_type", null: false
    t.integer "categorized_id",   null: false
  end

  add_index "comfy_cms_categorizations", ["category_id", "categorized_type", "categorized_id"], name: "index_cms_categorizations_on_cat_id_and_catd_type_and_catd_id", unique: true, using: :btree

  create_table "comfy_cms_files", force: true do |t|
    t.integer  "site_id",                                    null: false
    t.integer  "block_id"
    t.string   "label",                                      null: false
    t.string   "file_file_name",                             null: false
    t.string   "file_content_type",                          null: false
    t.integer  "file_file_size",                             null: false
    t.string   "description",       limit: 2048
    t.integer  "position",                       default: 0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comfy_cms_files", ["site_id", "block_id"], name: "index_comfy_cms_files_on_site_id_and_block_id", using: :btree
  add_index "comfy_cms_files", ["site_id", "file_file_name"], name: "index_comfy_cms_files_on_site_id_and_file_file_name", using: :btree
  add_index "comfy_cms_files", ["site_id", "label"], name: "index_comfy_cms_files_on_site_id_and_label", using: :btree
  add_index "comfy_cms_files", ["site_id", "position"], name: "index_comfy_cms_files_on_site_id_and_position", using: :btree

  create_table "comfy_cms_layouts", force: true do |t|
    t.integer  "site_id",                    null: false
    t.integer  "parent_id"
    t.string   "app_layout"
    t.string   "label",                      null: false
    t.string   "identifier",                 null: false
    t.text     "content"
    t.text     "css"
    t.text     "js"
    t.integer  "position",   default: 0,     null: false
    t.boolean  "is_shared",  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comfy_cms_layouts", ["parent_id", "position"], name: "index_comfy_cms_layouts_on_parent_id_and_position", using: :btree
  add_index "comfy_cms_layouts", ["site_id", "identifier"], name: "index_comfy_cms_layouts_on_site_id_and_identifier", unique: true, using: :btree

  create_table "comfy_cms_pages", force: true do |t|
    t.integer  "site_id",                        null: false
    t.integer  "layout_id"
    t.integer  "parent_id"
    t.integer  "target_page_id"
    t.string   "label",                          null: false
    t.string   "slug"
    t.string   "full_path",                      null: false
    t.text     "content_cache"
    t.integer  "position",       default: 0,     null: false
    t.integer  "children_count", default: 0,     null: false
    t.boolean  "is_published",   default: true,  null: false
    t.boolean  "is_shared",      default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comfy_cms_pages", ["parent_id", "position"], name: "index_comfy_cms_pages_on_parent_id_and_position", using: :btree
  add_index "comfy_cms_pages", ["site_id", "full_path"], name: "index_comfy_cms_pages_on_site_id_and_full_path", using: :btree

  create_table "comfy_cms_revisions", force: true do |t|
    t.string   "record_type", null: false
    t.integer  "record_id",   null: false
    t.text     "data"
    t.datetime "created_at"
  end

  add_index "comfy_cms_revisions", ["record_type", "record_id", "created_at"], name: "index_cms_revisions_on_rtype_and_rid_and_created_at", using: :btree

  create_table "comfy_cms_sites", force: true do |t|
    t.string  "label",                       null: false
    t.string  "identifier",                  null: false
    t.string  "hostname",                    null: false
    t.string  "path"
    t.string  "locale",      default: "en",  null: false
    t.boolean "is_mirrored", default: false, null: false
  end

  add_index "comfy_cms_sites", ["hostname"], name: "index_comfy_cms_sites_on_hostname", using: :btree
  add_index "comfy_cms_sites", ["is_mirrored"], name: "index_comfy_cms_sites_on_is_mirrored", using: :btree

  create_table "comfy_cms_snippets", force: true do |t|
    t.integer  "site_id",                    null: false
    t.string   "label",                      null: false
    t.string   "identifier",                 null: false
    t.text     "content"
    t.integer  "position",   default: 0,     null: false
    t.boolean  "is_shared",  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comfy_cms_snippets", ["site_id", "identifier"], name: "index_comfy_cms_snippets_on_site_id_and_identifier", unique: true, using: :btree
  add_index "comfy_cms_snippets", ["site_id", "position"], name: "index_comfy_cms_snippets_on_site_id_and_position", using: :btree

  create_table "debitcredit_accounts", force: true do |t|
    t.string   "name",              limit: 32,                                          null: false
    t.string   "type",              limit: 32,                                          null: false
    t.integer  "reference_id"
    t.string   "reference_type",    limit: 32
    t.decimal  "balance",                      precision: 20, scale: 2, default: 0.0,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "overdraft_enabled",                                     default: false, null: false
  end

  add_index "debitcredit_accounts", ["name", "reference_id", "reference_type"], name: "uindex", unique: true, using: :btree

  create_table "debitcredit_entries", force: true do |t|
    t.integer  "reference_id"
    t.string   "reference_type",   limit: 32
    t.string   "kind"
    t.string   "description",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "parent_entry_id"
    t.integer  "inverse_entry_id"
  end

  add_index "debitcredit_entries", ["created_at"], name: "index_debitcredit_entries_on_created_at", using: :btree
  add_index "debitcredit_entries", ["kind"], name: "index_debitcredit_entries_on_kind", using: :btree
  add_index "debitcredit_entries", ["parent_entry_id"], name: "index_debitcredit_entries_on_parent_entry_id", using: :btree
  add_index "debitcredit_entries", ["reference_id", "reference_type", "id"], name: "rindex", using: :btree

  create_table "debitcredit_items", force: true do |t|
    t.integer  "entry_id",                                          null: false
    t.integer  "account_id",                                        null: false
    t.boolean  "debit",                                             null: false
    t.string   "comment"
    t.decimal  "amount",     precision: 20, scale: 2, default: 0.0, null: false
    t.decimal  "balance",    precision: 20, scale: 2, default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "debitcredit_items", ["account_id"], name: "index_debitcredit_items_on_account_id", using: :btree
  add_index "debitcredit_items", ["debit"], name: "index_debitcredit_items_on_debit", using: :btree
  add_index "debitcredit_items", ["entry_id"], name: "index_debitcredit_items_on_entry_id", using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "demo_order_settings", force: true do |t|
    t.string   "subject"
    t.text     "description"
    t.string   "buyer_video_uid"
    t.string   "buyer_thumb_uid"
    t.integer  "buyer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deposits", force: true do |t|
    t.decimal  "amount",                      precision: 20, scale: 2
    t.integer  "user_id"
    t.string   "comment"
    t.string   "kind",             limit: 16
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "expires_at"
    t.datetime "expired_at"
    t.decimal  "amount_withdrawn",            precision: 20, scale: 2
    t.datetime "canceled_at"
  end

  add_index "deposits", ["expired_at", "expires_at"], name: "index_deposits_on_expired_at_and_expires_at", using: :btree
  add_index "deposits", ["kind"], name: "index_deposits_on_kind", using: :btree
  add_index "deposits", ["user_id"], name: "index_deposits_on_user_id", using: :btree

  create_table "devices", force: true do |t|
    t.integer  "user_id"
    t.string   "device_uid",            limit: 64
    t.string   "version",               limit: 32
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "push_token"
    t.boolean  "notifications_enabled",            default: true
    t.string   "kind",                  limit: 8
  end

  add_index "devices", ["user_id", "device_uid", "version"], name: "index_devices_on_user_id_and_device_uid_and_version", using: :btree
  add_index "devices", ["user_id", "kind", "notifications_enabled"], name: "index_devices_on_user_id_and_kind_and_notifications_enabled", using: :btree

  create_table "favorite_advisors", force: true do |t|
    t.integer  "user_id"
    t.integer  "advisor_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorite_advisors", ["advisor_id"], name: "index_favorite_advisors_on_advisor_id", using: :btree
  add_index "favorite_advisors", ["user_id", "advisor_id"], name: "index_favorite_advisors_on_user_id_and_advisor_id", unique: true, using: :btree
  add_index "favorite_advisors", ["user_id"], name: "index_favorite_advisors_on_user_id", using: :btree

  create_table "gift_cards", force: true do |t|
    t.integer  "card_template_id"
    t.integer  "buyer_id"
    t.string   "code",                       limit: 12
    t.string   "from_name"
    t.string   "from_email"
    t.string   "from_ip",                    limit: 15
    t.string   "to_name"
    t.string   "to_email"
    t.decimal  "price",                                   precision: 8,  scale: 2, null: false
    t.decimal  "credit_gained",                           precision: 8,  scale: 2, null: false
    t.datetime "purchased_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "send_on"
    t.string   "message",                    limit: 2048
    t.text     "stripe_data"
    t.string   "stripe_payment_token"
    t.string   "stripe_payment_email"
    t.string   "stripe_chargeid"
    t.string   "stripe_balance_transaction"
    t.decimal  "stripe_fee",                              precision: 20, scale: 2
    t.datetime "accounted_at"
    t.string   "error_message"
    t.datetime "processed_at"
    t.string   "buyer_token",                limit: 40
    t.datetime "redeemed_at"
    t.integer  "delayed_job_id"
    t.datetime "chargedback_at"
  end

  add_index "gift_cards", ["buyer_token"], name: "index_gift_cards_on_buyer_token", using: :btree
  add_index "gift_cards", ["chargedback_at"], name: "index_gift_cards_on_chargedback_at", using: :btree
  add_index "gift_cards", ["code"], name: "index_gift_cards_on_code", using: :btree
  add_index "gift_cards", ["redeemed_at"], name: "index_gift_cards_on_redeemed_at", using: :btree

  create_table "messages", force: true do |t|
    t.integer  "order_id"
    t.integer  "author_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["order_id", "created_at"], name: "index_messages_on_order_id_and_created_at", using: :btree

  create_table "order_refunds", force: true do |t|
    t.integer  "order_id"
    t.string   "reason",            limit: 2048
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "admin_description"
  end

  add_index "order_refunds", ["order_id"], name: "index_order_refunds_on_order_id", using: :btree

  create_table "orders", force: true do |t|
    t.integer  "advisor_id"
    t.text     "subject"
    t.text     "description"
    t.string   "buyer_video_uid"
    t.string   "advisor_video_uid"
    t.integer  "buyer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "buyer_nickname"
    t.boolean  "feedback_vote"
    t.text     "feedback_comment"
    t.datetime "feedback_created_at"
    t.integer  "messages_count",                                                        default: 0
    t.string   "state",                             limit: 32
    t.string   "token",                             limit: 54
    t.datetime "submitted_at"
    t.decimal  "price",                                        precision: 8,  scale: 2
    t.string   "buyer_thumb_uid"
    t.string   "advisor_thumb_uid"
    t.integer  "stripe_customer_id"
    t.integer  "stripe_payment_id"
    t.integer  "advisor_unread_messages_count",                                         default: 0
    t.integer  "buyer_unread_messages_count",                                           default: 0
    t.datetime "review_editable_until"
    t.string   "cancellation_reason"
    t.datetime "advisor_notified_about_pending_at"
    t.boolean  "test_work",                                                             default: false
    t.datetime "deadline_at"
    t.integer  "lock_entry_id"
    t.integer  "unlock_entry_id"
    t.integer  "fulfill_entry_id"
    t.datetime "fulfilled_at"
    t.datetime "cleared_at"
    t.decimal  "advisor_price",                                precision: 18, scale: 2
    t.decimal  "other_fees",                                   precision: 18, scale: 2
    t.integer  "clearance_entry_id"
    t.datetime "first_seen_at"
    t.string   "withdrawal_reason"
    t.integer  "unfulfill_entry_id"
    t.string   "rollback_reason"
    t.boolean  "rush_delivery",                                                         default: false
    t.datetime "rush_delivery_deadline_at"
    t.datetime "rush_delivery_missed_at"
    t.datetime "withdrawn_at"
    t.datetime "first_submitted_at"
    t.datetime "expired_at"
    t.datetime "canceled_at"
    t.datetime "expires_at"
    t.boolean  "auto_expired"
    t.decimal  "bonus",                                        precision: 8,  scale: 2
    t.decimal  "advisor_bonus",                                precision: 8,  scale: 2
    t.string   "buyer_os_version"
    t.string   "buyer_app_version"
    t.string   "buyer_os"
    t.string   "advisor_os"
    t.string   "advisor_os_version"
    t.string   "advisor_app_version"
    t.boolean  "counted_in_advisor_stats",                                              default: true,  null: false
    t.integer  "unclearance_entry_id"
    t.integer  "promotion_id"
    t.string   "advisor_ip",                        limit: 15
    t.string   "buyer_ip",                          limit: 15
  end

  add_index "orders", ["advisor_id", "buyer_id", "state", "submitted_at"], name: "works_for_advisors_order", using: :btree
  add_index "orders", ["advisor_id", "state", "updated_at"], name: "advisors_order", using: :btree
  add_index "orders", ["advisor_id"], name: "index_orders_on_advisor_id", using: :btree
  add_index "orders", ["buyer_id", "state", "expired_at"], name: "index_orders_on_buyer_id_and_state_and_expired_at", using: :btree
  add_index "orders", ["buyer_id", "state", "updated_at"], name: "buyers_order", using: :btree
  add_index "orders", ["buyer_id"], name: "index_orders_on_buyer_id", using: :btree
  add_index "orders", ["feedback_vote"], name: "index_orders_on_feedback_vote", using: :btree
  add_index "orders", ["fulfilled_at", "cleared_at"], name: "index_orders_on_fulfilled_at_and_cleared_at", using: :btree
  add_index "orders", ["promotion_id"], name: "index_orders_on_promotion_id", using: :btree
  add_index "orders", ["state", "auto_expired"], name: "index_orders_on_state_and_auto_expired", using: :btree
  add_index "orders", ["state", "rush_delivery_deadline_at", "rush_delivery_missed_at"], name: "rush_delivery_lookups", using: :btree
  add_index "orders", ["submitted_at", "advisor_id"], name: "index_orders_on_submitted_at_and_advisor_id", using: :btree

  create_table "pages", force: true do |t|
    t.text     "text"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["slug"], name: "index_pages_on_slug", unique: true, using: :btree

  create_table "promotions", force: true do |t|
    t.boolean  "is_active",                default: false, null: false
    t.datetime "start_at",                                 null: false
    t.datetime "stop_at",                                  null: false
    t.integer  "advisor_fee_percent",      default: 40,    null: false
    t.integer  "advisor_rush_fee_percent", default: 45,    null: false
    t.string   "banner_uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchase_transactions", force: true do |t|
    t.integer  "purchase_id"
    t.decimal  "price_paid",                        precision: 18, scale: 2
    t.string   "productid"
    t.string   "deviceid"
    t.datetime "accounted_at"
    t.decimal  "credits_gained",                    precision: 18, scale: 2
    t.string   "remote_transactionid"
    t.string   "apple_productid"
    t.string   "apple_cancellation_date"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",                limit: 8
    t.datetime "chargedback_at"
  end

  add_index "purchase_transactions", ["chargedback_at"], name: "index_purchase_transactions_on_chargedback_at", using: :btree
  add_index "purchase_transactions", ["provider"], name: "index_purchase_transactions_on_provider", using: :btree
  add_index "purchase_transactions", ["purchase_id"], name: "index_purchase_transactions_on_purchase_id", using: :btree
  add_index "purchase_transactions", ["remote_transactionid"], name: "index_purchase_transactions_on_remote_transactionid", using: :btree

  create_table "purchases", force: true do |t|
    t.integer  "user_id"
    t.text     "purchase_receipt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "receipt_type"
    t.string   "provider",         limit: 8
    t.string   "signature",        limit: 4096
  end

  add_index "purchases", ["provider"], name: "index_purchases_on_provider", using: :btree
  add_index "purchases", ["user_id"], name: "index_purchases_on_user_id", using: :btree

  create_table "push_logs", force: true do |t|
    t.integer  "user_id"
    t.string   "message"
    t.text     "raw_data"
    t.text     "push_tokens"
    t.string   "provider",      limit: 8
    t.text     "response"
    t.integer  "sent_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sending_count"
  end

  add_index "push_logs", ["sent_count"], name: "index_push_logs_on_sent_count", using: :btree
  add_index "push_logs", ["user_id", "provider"], name: "index_push_logs_on_user_id_and_provider", using: :btree

  create_table "reconciliations", force: true do |t|
    t.decimal  "amount",                             precision: 20, scale: 2
    t.integer  "reconciliation_entry_id"
    t.string   "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",                limit: 32
  end

  create_table "redemptions", force: true do |t|
    t.integer  "user_id"
    t.integer  "gift_card_id"
    t.decimal  "credit_gained",             precision: 8, scale: 2, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "from_ip",        limit: 15
    t.datetime "chargedback_at"
  end

  add_index "redemptions", ["chargedback_at"], name: "index_redemptions_on_chargedback_at", using: :btree
  add_index "redemptions", ["user_id"], name: "index_redemptions_on_user_id", using: :btree

  create_table "settings", force: true do |t|
    t.boolean "allow_new_advisor_application", default: true,  null: false
    t.string  "min_ios_version"
    t.string  "min_android_version"
    t.boolean "allow_autopayout",              default: false, null: false
    t.boolean "show_banner"
    t.string  "banner_uid"
    t.string  "banner_link"
    t.boolean "pass_session_token",            default: true,  null: false
    t.boolean "enforce_clean_desk_policy",     default: true,  null: false
    t.integer "max_queue_size",                default: 10,    null: false
    t.integer "max_rush_jobs",                 default: 5,     null: false
  end

  create_table "stripe_credits", force: true do |t|
    t.integer  "user_id"
    t.text     "stripe_data"
    t.string   "stripe_payment_token"
    t.string   "stripe_payment_email"
    t.decimal  "amount_paid",                           precision: 20, scale: 2
    t.decimal  "credit_gained",                         precision: 20, scale: 2
    t.decimal  "credit_requested",                      precision: 20, scale: 2
    t.string   "stripe_chargeid"
    t.string   "stripe_balance_transaction"
    t.decimal  "stripe_fee",                            precision: 20, scale: 2
    t.datetime "accounted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "processed_at"
    t.string   "error_message"
    t.string   "from_ip",                    limit: 15
    t.datetime "chargedback_at"
  end

  add_index "stripe_credits", ["chargedback_at"], name: "index_stripe_credits_on_chargedback_at", using: :btree
  add_index "stripe_credits", ["user_id"], name: "index_stripe_credits_on_user_id", using: :btree

  create_table "stripe_customers", force: true do |t|
    t.integer  "user_id"
    t.string   "token"
    t.string   "stripe_customer_id"
    t.string   "email"
    t.text     "subscriptions"
    t.text     "cards"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "card_last4"
    t.string   "card_brand"
    t.string   "card_id"
  end

  add_index "stripe_customers", ["user_id"], name: "index_stripe_customers_on_user_id", using: :btree

  create_table "stripe_payins", force: true do |t|
    t.string   "details"
    t.decimal  "amount",     precision: 20, scale: 2, default: 0.0, null: false
    t.date     "paid_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stripe_payments", force: true do |t|
    t.integer  "seller_id"
    t.integer  "buyer_id"
    t.integer  "amount_cents"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "stripe_charge_id"
    t.string   "stripe_customer_id"
    t.string   "balance_transaction"
    t.datetime "paid_at"
    t.text     "card"
    t.integer  "application_fee_cents"
    t.string   "stripe_charge_token"
    t.integer  "net_cents"
    t.integer  "stripe_fee_cents"
    t.integer  "total_fee_cents"
    t.text     "fee_details"
    t.string   "state"
  end

  add_index "stripe_payments", ["buyer_id"], name: "index_stripe_payments_on_buyer_id", using: :btree
  add_index "stripe_payments", ["seller_id"], name: "index_stripe_payments_on_seller_id", using: :btree

  create_table "support_messages", force: true do |t|
    t.string   "email"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "sender_os"
    t.string   "sender_os_version"
    t.string   "sender_app_version"
  end

  add_index "support_messages", ["user_id"], name: "index_support_messages_on_user_id", using: :btree

  create_table "titles", force: true do |t|
    t.text     "body"
    t.string   "title"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_logs", force: true do |t|
    t.integer  "user_id"
    t.string   "uid",         limit: 512
    t.string   "comment"
    t.datetime "uploaded_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_logs", ["user_id", "uploaded_at"], name: "index_user_logs_on_user_id_and_uploaded_at", using: :btree

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "encrypted_password",                  default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "stripe_access_token"
    t.string   "stripe_refresh_token"
    t.string   "stripe_publishable_key"
    t.string   "stripe_user_id"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "facebook_id",              limit: 8
    t.string   "google_id"
    t.string   "full_name"
    t.string   "phone_number"
    t.string   "session_token"
    t.string   "nickname"
    t.string   "facebook_token"
    t.text     "details"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "zip"
    t.date     "date_of_birth"
    t.string   "address"
    t.string   "social_avatar_url"
    t.string   "avatar_uid"
    t.datetime "suspended_at"
    t.string   "push_token"
    t.string   "intention",                limit: 32
    t.text     "stripe_account"
    t.boolean  "superbuyer"
    t.string   "country_code",             limit: 32
    t.string   "gender",                   limit: 3
    t.string   "campaign_medium"
    t.string   "campaign_source"
    t.string   "campaign_name"
    t.string   "fb_adset_name"
    t.boolean  "mixpanel_alias_created",              default: false
    t.string   "fb_ad_name"
    t.string   "fb_ad_id"
    t.string   "android_push_token"
    t.datetime "can_become_advisor_until"
    t.string   "paypal_email"
    t.text     "paypal_data"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["facebook_id"], name: "index_users_on_facebook_id", using: :btree
  add_index "users", ["facebook_token"], name: "index_users_on_facebook_token", unique: true, using: :btree
  add_index "users", ["google_id"], name: "index_users_on_google_id", using: :btree
  add_index "users", ["nickname"], name: "index_users_on_nickname", using: :btree
  add_index "users", ["paypal_email"], name: "index_users_on_paypal_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["session_token"], name: "index_users_on_session_token", unique: true, using: :btree

  create_table "withdrawals", force: true do |t|
    t.integer  "advisor_id"
    t.decimal  "amount",                               precision: 20, scale: 2
    t.string   "state",                   limit: 16
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "comment"
    t.decimal  "paypal_payout_fee",                    precision: 20, scale: 2
    t.decimal  "net_amount",                           precision: 20, scale: 2
    t.datetime "autopayout_attempted_at"
    t.datetime "autopayout_succeed_at"
    t.datetime "autopayout_reported_at"
    t.string   "last_auto_payout_state",  limit: 2048
  end

  add_index "withdrawals", ["autopayout_attempted_at", "autopayout_reported_at", "last_auto_payout_state"], name: "autopayout_report", using: :btree
  add_index "withdrawals", ["state", "advisor_id"], name: "index_withdrawals_on_state_and_advisor_id", using: :btree

end
