class CreateTitles < ActiveRecord::Migration
  def change
    create_table :titles do |t|
      t.text :body
      t.string :title
      t.string :link
      t.timestamps
    end
  end
end
